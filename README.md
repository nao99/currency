Currency project

For this project start you need to install docker (or use native postgresql)
If you have docker:<br>
1) Install docker-compose
2) Create and set .env file by .env.dist and move it and docker-compose.yml to somewhere
if you want it (i prefer /home/user/docker/project_name/)
3) Create and set application.properties by application.properties.dist
4) Run docker-compose in the docker-compose.yml folder
5) Copy migration sql scripts to the docker container and run its to there

```console
foo@bar:~$ sudo docker-compose up -d
foo@bar:~$ sudo docker cp path_to_the_project/currency/src/main/java/currency/migration/ gpon_postgres:/sql_migrations
```
Create database in the psq and migrate it
```console
foo@bar:~$ sudo docker exec -it currency_postgres /bin/bash
root@container_id:/# psql -U dbuser
dbuser=#: create database currency;
dbuser=# \q
root@container_id:/# psql -U dbuser -d currency -a -f sql_migrations/Version1.sql
root@container_id:/# psql -U dbuser -d currency -a -f sql_migrations/Version2.sql
root@container_id:/# psql -U dbuser -d currency -a -f sql_migrations/Version3.sql
```

Data for authenticate: <br>
<b>login</b>: testuser <br>
<b>password</b>: helloworld

Congratulations! You have tuned database <br>
Now you need just to run the application and enjoy (spit and hate) it

