package currency.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "currency_course")
public class CurrencyCourse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "source_id", nullable = false)
    private Currency source;

    @ManyToOne
    @JoinColumn(name = "target_id", nullable = false)
    private Currency target;

    @Column(name = "value", nullable = false)
    private Double value;

    @Column(name = "created_at", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date createdAt;

    /**
     * CurrencyCourse default constructor
     */
    public CurrencyCourse() {}

    /**
     * CurrencyCourse constructor
     */
    public CurrencyCourse(Currency source, Currency target, Double value, Date createdAt) {
        this.source = source;
        this.target = target;
        this.value = value;
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return this.id;
    }

    public CurrencyCourse setId(Integer id) {
        this.id = id;
        return this;
    }

    public Currency getSource() {
        return this.source;
    }

    public CurrencyCourse setSource(Currency source) {
        this.source = source;
        return this;
    }

    public Currency getTarget() {
        return this.target;
    }

    public CurrencyCourse setTarget(Currency target) {
        this.target = target;
        return this;
    }

    public Double getValue() {
        return this.value;
    }

    public CurrencyCourse setValue(Double value) {
        this.value = value;
        return this;
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public CurrencyCourse setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
        return this;
    }
}
