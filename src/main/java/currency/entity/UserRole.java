package currency.entity;

import javax.persistence.*;

@Entity
@Table(name = "user_role")
public class UserRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "role_id", nullable = false)
    private Role role;

    /**
     * UserRole default constructor
     */
    public UserRole() {}

    /**
     * UserRole constructor
     */
    public UserRole(User user, Role role) {
        this.user = user;
        this.role = role;
    }

    public Integer getId() {
        return this.id;
    }

    public UserRole setId(Integer id) {
        this.id = id;
        return this;
    }

    public User getUser() {
        return this.user;
    }

    public UserRole setUser(User user) {
        this.user = user;
        return this;
    }

    public Role getRole() {
        return this.role;
    }

    public UserRole setRole(Role role) {
        this.role = role;
        return this;
    }
}
