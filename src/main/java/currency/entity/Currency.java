package currency.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "currency")
public class Currency {
    public static final String RUSSIAN_CHAR_CODE = "RUB";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * Currency id from cbr
     * E.g. R01010
     */
    @Column(name = "currency_id", length = 8, nullable = false)
    private String currencyId;

    @Column(name = "num_code", length = 3, nullable = false)
    private String numCode;

    @Column(name = "char_code", length = 3, nullable = false)
    private String charCode;

    @Column(name = "name", length = 65, nullable = false)
    private String name;

    @OneToMany(mappedBy = "source", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    private List<CurrencyCourse> courses;

    /**
     * Currency default constructor
     */
    public Currency() {}

    /**
     * Currency constructor
     */
    public Currency(String currencyId, String numCode, String charCode, String name) {
        this.currencyId = currencyId;
        this.numCode = numCode;
        this.charCode = charCode;
        this.name = name;
    }

    public Integer getId() {
        return this.id;
    }

    public Currency setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getCurrencyId() {
        return this.currencyId;
    }

    public Currency setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
        return this;
    }

    public String getNumCode() {
        return this.numCode;
    }

    public Currency setNumCode(String numCode) {
        this.numCode = numCode;
        return this;
    }

    public String getCharCode() {
        return this.charCode;
    }

    public Currency setCharCode(String charCode) {
        this.charCode = charCode;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public Currency setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (!Currency.class.isAssignableFrom(obj.getClass())) {
            return false;
        }

        Currency currency = (Currency) obj;
        if (!this.id.equals(currency.id)) {
            return false;
        }

        if (!this.currencyId.equals(currency.currencyId)) {
            return false;
        }

        if (!this.numCode.equals(currency.numCode)) {
            return false;
        }

        if (!this.charCode.equals(currency.charCode)) {
            return false;
        }

        if (!this.name.equals(currency.name)) {
            return false;
        }

        return true;
    }
}
