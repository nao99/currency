package currency.entity;

import javax.persistence.*;

@Entity
@Table(name = "role")
public class Role {
    public static final Integer ROLE_ADMIN = 1;
    public static final Integer ROLE_USER  = 2;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name", length = 65, nullable = false)
    private String name;

    /**
     * Role default constructor
     */
    public Role() {}

    public Integer getId() {
        return this.id;
    }

    public Role setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public Role setName(String name) {
        this.name = name;
        return this;
    }
}
