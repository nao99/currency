package currency.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "user_conversion")
public class UserConversion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "currency_course_id", nullable = false)
    private CurrencyCourse course;

    @Column(name = "amount", nullable = false)
    private Double amount;

    @Column(name = "sum", nullable = false)
    private Double sum;

    @Column(name = "created_at", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    /**
     * UserConversion default constructor
     */
    public UserConversion() {}

    /**
     * UserConversion constructor
     */
    public UserConversion(User user, CurrencyCourse course, Double amount, Double sum) {
        this.user = user;
        this.course = course;
        this.amount = amount;
        this.sum = sum;
    }

    public Integer getId() {
        return this.id;
    }

    public UserConversion setId(Integer id) {
        this.id = id;
        return this;
    }

    public User getUser() {
        return this.user;
    }

    public UserConversion setUser(User user) {
        this.user = user;
        return this;
    }

    public CurrencyCourse getCourse() {
        return this.course;
    }

    public UserConversion setCourse(CurrencyCourse course) {
        this.course = course;
        return this;
    }

    public Double getAmount() {
        return this.amount;
    }

    public UserConversion setAmount(Double amount) {
        this.amount = amount;
        return this;
    }

    public Double getSum() {
        return this.sum;
    }

    public UserConversion setSum(Double sum) {
        this.sum = sum;
        return this;
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public UserConversion setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    @PrePersist
    private void updateTimestamps() {
        this.createdAt = new Date();
    }
}
