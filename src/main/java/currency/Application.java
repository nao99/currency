package currency;

import currency.job.InitApplicationJob;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);

        Thread initThread = new Thread(context.getBean(InitApplicationJob.class));
        initThread.run();
    }
}
