package currency.repository;

import currency.entity.Currency;
import currency.repository.custom.CurrencyRepositoryCustom;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CurrencyRepository extends CrudRepository<Currency, Integer>, CurrencyRepositoryCustom {
    /**
     * Finds Currency by charCode if exists
     */
    public Optional<Currency> findByCharCode(String charCode);
}
