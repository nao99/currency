package currency.repository.custom;

import currency.entity.UserConversion;
import currency.model.dto.request.UserConversionGetDto;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class UserConversionRepositoryImpl implements UserConversionRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<UserConversion> findUserConversions(UserConversionGetDto getDto) {
        CriteriaBuilder cb = this.em.getCriteriaBuilder();

        CriteriaQuery<UserConversion> query = cb.createQuery(UserConversion.class);
        Root<UserConversion> root = query.from(UserConversion.class);

        if (getDto.getUser() != null) {
            query.where(cb.equal(root.get("user"), getDto.getUser()));
        }

        if (getDto.getCurrencyCourse() != null) {
            query.where(cb.equal(root.get("currencyCourse"), getDto.getCurrencyCourse()));
        }

        if (getDto.getAmount() != null) {
            query.where(cb.equal(root.get("amount"), getDto.getAmount()));
        }

        if (getDto.getSum() != null) {
            query.where(cb.equal(root.get("sum"), getDto.getSum()));
        }

        if (getDto.getCreatedAt() != null) {
            query.where(cb.equal(root.get("createdAt"), getDto.getCreatedAt()));
        }

        return this.em.createQuery(query)
                .setMaxResults(getDto.getLimit())
                .setFirstResult(getDto.getOffset())
                .getResultList();
    }
}
