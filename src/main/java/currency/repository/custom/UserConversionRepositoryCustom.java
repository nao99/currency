package currency.repository.custom;

import currency.entity.UserConversion;
import currency.model.dto.request.UserConversionGetDto;

import java.util.List;

public interface UserConversionRepositoryCustom {
    public List<UserConversion> findUserConversions(UserConversionGetDto getDto);
}
