package currency.repository.custom;

import currency.entity.Currency;

import java.util.Date;
import java.util.List;

public interface CurrencyRepositoryCustom {
    /**
     * Finds Currencies that date less than need
     */
    public List<Currency> findExpiredCurrencies(Date createdAt);
}
