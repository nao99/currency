package currency.repository.custom;

import currency.entity.Currency;
import currency.entity.CurrencyCourse;
import currency.model.util.DateTimeRange;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;

public class CurrencyCourseRepositoryImpl implements CurrencyCourseRepositoryCustom {
    @PersistenceContext
    private EntityManager em;

    @Override
    public CurrencyCourse findFreshCourse(Currency source, Currency target, DateTimeRange range) {
        CriteriaBuilder cb = this.em.getCriteriaBuilder();

        CriteriaQuery<CurrencyCourse> query = cb.createQuery(CurrencyCourse.class).distinct(true);
        Root<CurrencyCourse> course = query.from(CurrencyCourse.class);

        query
                .where(cb.and(
                        cb.equal(course.get("source"), source),
                        cb.equal(course.get("target"), target),
                        cb.or(
                                cb.greaterThanOrEqualTo(course.get("createdAt"), range.getStartAt()),
                                cb.lessThan(course.get("createdAt"), range.getEndAt())
                        )
                ));

        try {
            return this.em.createQuery(query).getSingleResult();

        } catch (NoResultException e) {
            return null;
        }
    }
}
