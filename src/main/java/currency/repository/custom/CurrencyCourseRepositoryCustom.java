package currency.repository.custom;

import currency.entity.Currency;
import currency.entity.CurrencyCourse;
import currency.model.util.DateTimeRange;

public interface CurrencyCourseRepositoryCustom {
    /**
     * Finds today's or tomorrow's (by cbr opportunities)
     * CurrencyCourse by source and target if exists
     */
    public CurrencyCourse findFreshCourse(Currency source, Currency target, DateTimeRange range);
}
