package currency.repository.custom;

import currency.entity.Currency;
import currency.entity.CurrencyCourse;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.Date;
import java.util.List;

public class CurrencyRepositoryImpl implements CurrencyRepositoryCustom {
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Currency> findExpiredCurrencies(Date createdAt) {
        CriteriaBuilder cb = this.em.getCriteriaBuilder();

        CriteriaQuery<Currency> query = cb.createQuery(Currency.class).distinct(true);
        Root<Currency> currency = query.from(Currency.class);

        Join<Currency, CurrencyCourse> course = currency.join("courses", JoinType.LEFT);
        course.on(
                cb.equal(currency.get("id"), course.get("source").get("id"))
        );

        query
                .where(cb.lessThan(course.get("createdAt"), createdAt))
                .where(cb.and(
                        cb.notEqual(currency.get("charCode"), Currency.RUSSIAN_CHAR_CODE),
                        cb.or(
                                cb.isNull(course.get("id"))
                        )
                ));

        return this.em.createQuery(query).getResultList();
    }
}
