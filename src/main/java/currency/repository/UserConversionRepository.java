package currency.repository;

import currency.entity.UserConversion;
import currency.repository.custom.UserConversionRepositoryCustom;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserConversionRepository extends CrudRepository<UserConversion, Integer>,
        UserConversionRepositoryCustom {
}
