package currency.repository;

import currency.entity.CurrencyCourse;
import currency.repository.custom.CurrencyCourseRepositoryCustom;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrencyCourseRepository extends CrudRepository<CurrencyCourse, Integer>,
        CurrencyCourseRepositoryCustom {
}
