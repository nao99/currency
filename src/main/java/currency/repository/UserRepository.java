package currency.repository;

import currency.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    /**
     * Finds User by login if exists
     */
    public Optional<User> findByLogin(String login);
}
