package currency.service;

import currency.entity.User;
import currency.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {
    private UserRepository userRepository;

    /**
     * UserService constructor
     */
    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUser(String login) {
        Optional<User> user = this.userRepository.findByLogin(login);
        return user.orElse(null);
    }
}
