package currency.service;

import currency.entity.User;
import currency.exception.CurrencyConflictException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class SecurityService {
    private UserService userService;

    /**
     * SecurityService constructor
     */
    @Autowired
    public SecurityService(UserService userService) {
        this.userService = userService;
    }

    public User getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        User user = this.userService.getUser(authentication.getName());
        if (user == null) {
            throw new CurrencyConflictException("User not found");
        }

        return user;
    }
}
