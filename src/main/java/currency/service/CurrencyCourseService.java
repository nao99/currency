package currency.service;

import currency.entity.Currency;
import currency.entity.CurrencyCourse;
import currency.exception.CurrencyBadRequestException;
import currency.exception.CurrencyConflictException;
import currency.exception.CurrencyNotFoundException;
import currency.exception.CurrencyUnprocessableEntityException;
import currency.model.dto.request.CurrencyCourseAddDto;
import currency.model.util.DateTimeRange;
import currency.repository.CurrencyCourseRepository;
import currency.service.cbr.CbrApiService;
import currency.service.util.XmlParserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.validation.ConstraintViolation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

@Service
public class CurrencyCourseService {
    private EntityManager em;

    private CbrApiService cbrApiService;

    private XmlParserService xmlParserService;

    private CurrencyService currencyService;

    private CurrencyCourseRepository currencyCourseRepository;

    private Validator validator;

    /**
     * CurrencyCourseService constructor
     */
    @Autowired
    public CurrencyCourseService(EntityManagerFactory entityManagerFactory, CbrApiService cbrApiService,
                                 XmlParserService xmlParserService, CurrencyService currencyService,
                                 CurrencyCourseRepository currencyCourseRepository, ValidatorFactory validatorFactory) {

        this.em = entityManagerFactory.createEntityManager();
        this.cbrApiService = cbrApiService;
        this.xmlParserService = xmlParserService;
        this.currencyService = currencyService;

        this.currencyCourseRepository = currencyCourseRepository;
        this.validator = validatorFactory.getValidator();
    }

    /**
     * Get today or tomorrow CurrencyCourse
     */
    public CurrencyCourse getCourse(Currency source, Currency target) {
        return this.currencyCourseRepository
                .findFreshCourse(source, target, DateTimeRange.getOneDayRange());
    }

    /**
     * Creates a new CurrencyCourse by
     * two already existed another courses
     *
     * Warning: Both this courses must have the Russian Currency as target
     */
    public CurrencyCourse calculateCourse(CurrencyCourse source, CurrencyCourse target) {
        if (!source.getTarget().equals(target.getTarget())) {
            throw new CurrencyBadRequestException("Courses is not related to the one currency");
        }

        Double value = source.getValue() / target.getValue();
        value = round(value, 5);

        CurrencyCourseAddDto addDto = new CurrencyCourseAddDto(
                source.getSource(),
                target.getSource(),
                value,
                source.getCreatedAt()
        );

        return this.addCourse(addDto);
    }

    @Transactional
    public CurrencyCourse addCourse(CurrencyCourseAddDto addDto) {
        CurrencyCourse course = null;
        try {
            this.em.getTransaction().begin();

            course = new CurrencyCourse(
                    addDto.getSource(),
                    addDto.getTarget(),
                    addDto.getValue(),
                    addDto.getCreatedAt()
            );

            this.em.persist(course);
            this.em.flush();

            this.em.getTransaction().commit();

        } catch (Exception e) {
            this.em.getTransaction().rollback();
            throw e;
        }

        return course;
    }

    public CurrencyCourse addCurrencyCourse(Currency currency, DateTimeRange range) {
        Document xml = this.getCoursesXml(currency.getCurrencyId(), range);

        Currency russianCurrency = this.currencyService.getCurrency(Currency.RUSSIAN_CHAR_CODE);
        if (russianCurrency == null) {
            throw new CurrencyConflictException("Russian currency not found");
        }

        CurrencyCourseAddDto addDto = this.getCourseByXml(currency, russianCurrency, xml);

        return this.addCourse(addDto);
    }

    public CurrencyCourseAddDto getCourseByXml(Currency source, Currency target, Document xml) {
        NodeList nodes = xml.getElementsByTagName("Record");

        if (nodes.getLength() == 0) {
            throw new CurrencyNotFoundException("Currency course not found");
        }

        Node node = nodes.item(nodes.getLength() - 1);
        NodeList children = node.getChildNodes();

        Map<String, String> nodeValues = new HashMap<>();
        for (int j = 0; j < children.getLength(); j++) {
            Node childNode = children.item(j);

            nodeValues.put(
                    childNode.getNodeName(),
                    childNode.getFirstChild().getNodeValue()
            );
        }

        Double fetchedValue = null;
        Date date = null;

        try {
            String valueStr = nodeValues.get("Value").replace(',', '.');
            String dateStr = node.getAttributes().getNamedItem("Date").getNodeValue();

            Double value = round(Double.parseDouble(valueStr), 4);
            Integer nominal = Integer.parseInt(nodeValues.get("Nominal"));

            fetchedValue = value / nominal;
            date = new SimpleDateFormat(DateTimeRange.DD_MM_YYY_DOT_FORMAT).parse(dateStr);

        } catch (Exception e) {
            throw new CurrencyUnprocessableEntityException(e.getMessage(), e.getCause());
        }

        CurrencyCourseAddDto currencyCourse = new CurrencyCourseAddDto(
                source,
                target,
                fetchedValue,
                date
        );

        Set<ConstraintViolation<CurrencyCourseAddDto>> violations = this.validator.validate(currencyCourse);
        if (!violations.isEmpty()) {
            throw new ValidationException(violations.toString());
        }

        return currencyCourse;
    }

    public CurrencyCourse addTodayCourse(Currency currency) {
        return this.addCurrencyCourse(
                currency,
                DateTimeRange.getOneDayRange()
        );
    }

    /**
     * Gets xml with available currency courses
     * from cbr api by period
     */
    public Document getCoursesXml(String currencyId, DateTimeRange range) {
        String xmlStr = this.cbrApiService.getCurrencyCourse(currencyId, range);

        return this.xmlParserService.parseXML(xmlStr);
    }

    public static double round(double value, int places) {
        double scale = Math.pow(10, places);
        return Math.round(value * scale) / scale;
    }
}
