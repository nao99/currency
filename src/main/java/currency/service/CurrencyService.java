package currency.service;

import currency.entity.Currency;
import currency.model.dto.request.CurrencyAddDto;
import currency.repository.CurrencyRepository;
import currency.service.cbr.CbrApiService;
import currency.service.util.XmlParserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.*;

@Service
public class CurrencyService {
    private EntityManager em;

    private CbrApiService cbrApiService;

    private XmlParserService xmlParserService;

    private CurrencyRepository currencyRepository;

    private Validator validator;

    /**
     * CurrencyService constructor
     */
    @Autowired
    public CurrencyService(EntityManagerFactory entityManagerFactory, CbrApiService cbrApiService,
                           XmlParserService xmlParserService,CurrencyRepository currencyRepository,
                           ValidatorFactory validatorFactory) {

        this.em = entityManagerFactory.createEntityManager();
        this.cbrApiService = cbrApiService;
        this.xmlParserService = xmlParserService;

        this.currencyRepository = currencyRepository;
        this.validator = validatorFactory.getValidator();
    }

    public Currency getCurrency(Integer id) {
        return this.currencyRepository
                .findById(id)
                .orElse(null);
    }

    public Currency getCurrency(String charCode) {
        return this.currencyRepository
                .findByCharCode(charCode)
                .orElse(null);
    }

    public Iterable<Currency> getCurrencies() {
        return this.currencyRepository.findAll();
    }

    /**
     * Gets Currencies that have not a CurrencyCourses
     * on the today
     */
    public List<Currency> getExpiredCurrencies(Date today) {
        return this.currencyRepository.findExpiredCurrencies(today);
    }

    public Map<String, CurrencyAddDto> getCurrenciesByXml(Document xml) {
        Map<String, CurrencyAddDto> currencies = new HashMap<>();
        NodeList nodes = xml.getElementsByTagName("Valute");

        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Node currencyId = node.getAttributes().getNamedItem("ID");

            if (currencyId == null) {
                continue;
            }

            Map<String, String> nodeValues = new HashMap<>();
            NodeList children = node.getChildNodes();

            for (int j = 0; j < children.getLength(); j++) {
                Node childNode = children.item(j);

                nodeValues.put(
                        childNode.getNodeName(),
                        childNode.getFirstChild().getNodeValue()
                );
            }

            CurrencyAddDto addDto = new CurrencyAddDto(
                    currencyId.getNodeValue(),
                    nodeValues.get("NumCode"),
                    nodeValues.get("CharCode"),
                    nodeValues.get("Name")
            );

            Set<ConstraintViolation<CurrencyAddDto>> violations = this.validator.validate(addDto);
            if (violations.isEmpty()) {
                currencies.put(addDto.getCurrencyId(), addDto);
            }
        }

        return currencies;
    }

    /**
     * Gets xml with available currencies
     * from cbr api
     */
    public Document getCurrenciesXml() {
        String xmlStr = this.cbrApiService.getCurrencies();

        return this.xmlParserService.parseXML(xmlStr);
    }

    /**
     * Defines a nonexistent Currencies in the database
     * and returns CurrencyAddDtos for its creating
     */
    public List<CurrencyAddDto> defineNewCurrencies(Iterable<Currency> currencies,
                                                            Map<String, CurrencyAddDto> addDtos) {

        currencies.forEach(currency -> {
            if (addDtos.get(currency.getCurrencyId()) != null) {
                addDtos.remove(currency.getCurrencyId());
            }
        });

        return new ArrayList<>(addDtos.values());
    }

    @Transactional
    public void addCurrencies(List<CurrencyAddDto> addDtos) {
        int persistentQuantity = 0;

        try {
            this.em.getTransaction().begin();

            for (CurrencyAddDto addDto : addDtos) {
                Currency currency = new Currency(
                        addDto.getCurrencyId(),
                        addDto.getNumCode(),
                        addDto.getCharCode(),
                        addDto.getName()
                );

                this.em.persist(currency);
                persistentQuantity++;

                if (persistentQuantity % 100 == 0) {
                    this.em.flush();
                }
            }

            this.em.flush();
            this.em.getTransaction().commit();

        } catch (RuntimeException e) {
            this.em.getTransaction().rollback();
            throw e;
        }
    }
}
