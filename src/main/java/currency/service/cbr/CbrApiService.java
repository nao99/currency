package currency.service.cbr;

import currency.exception.cbr.CbrApiException;
import currency.model.util.DateTimeRange;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Service
public class CbrApiService {
    private String url;
    private RestTemplate rest;
    private HttpHeaders headers;

    /**
     * CbrApiService constructor
     */
    public CbrApiService() {
        this.url = "http://www.cbr.ru/";
        this.rest = new RestTemplate();
        this.headers = new HttpHeaders();

        this.headers.add("Content-Type", "application/xml");
        this.headers.add("Accept", "/");
    }

    /**
     * Sends request to the cbr api for getting
     * available currencies
     */
    public String getCurrencies() {
        ResponseEntity<String> responseEntity = null;
        try {
            HttpEntity<String> requestEntity = new HttpEntity<>(null, this.headers);

            responseEntity = this.rest.exchange(
                    String.format(this.url + "%s", "scripts/XML_daily_eng.asp"),
                    HttpMethod.GET,
                    requestEntity,
                    String.class
            );

        } catch (RuntimeException e) {
            throw new CbrApiException(e.getMessage(), e.getCause());
        }

        return responseEntity.getBody();
    }

    /**
     * Sends request to the cbr api for getting
     * currency course to the ruble for given period
     */
    public String getCurrencyCourse(String currencyId, DateTimeRange range) {
        ResponseEntity<String> responseEntity = null;

        Map<String, String> formattedStringRange = range.toFormattedStringRange(DateTimeRange.DD_MM_YYY_SLASH_FORMAT);
        String startAt = formattedStringRange.get("startAt");
        String endAt = formattedStringRange.get("endAt");

        String endPointUrl = String.format(
                "%s/scripts/XML_dynamic.asp?date_req1=%s&date_req2=%s&VAL_NM_RQ=%s",
                this.url,
                startAt,
                endAt,
                currencyId
        );

        try {
            HttpEntity<String> requestEntity = new HttpEntity<>(null, this.headers);

            responseEntity = this.rest.exchange(
                    endPointUrl,
                    HttpMethod.GET,
                    requestEntity,
                    String.class
            );

        } catch (RuntimeException e) {
            throw new CbrApiException(e.getMessage(), e.getCause());
        }

        return responseEntity.getBody();
    }
}
