package currency.service;

import currency.entity.Currency;
import currency.entity.CurrencyCourse;
import currency.entity.User;
import currency.entity.UserConversion;
import currency.model.dto.request.UserConversionGetDto;
import currency.repository.UserConversionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

@Service
public class UserConversionService {
    private EntityManager em;

    private CurrencyService currencyService;

    private CurrencyCourseService currencyCourseService;

    private UserConversionRepository userConversionRepository;

    /**
     * UserConversionService constructor
     */
    @Autowired
    public UserConversionService(EntityManagerFactory entityManagerFactory, CurrencyService currencyService,
                                 CurrencyCourseService currencyCourseService,
                                 UserConversionRepository userConversionRepository) {

        this.em = entityManagerFactory.createEntityManager();
        this.currencyService = currencyService;
        this.currencyCourseService = currencyCourseService;

        this.userConversionRepository = userConversionRepository;
    }

    public List<UserConversion> getConversions(UserConversionGetDto conversionGetDto) {
        return this.userConversionRepository.findUserConversions(conversionGetDto);
    }

    @Transactional
    public UserConversion addConversion(User user, Currency source, Currency target, Double amount) {
        CurrencyCourse course = this.currencyCourseService.getCourse(source, target);
        if (course == null) {
            Currency russianCurrency = this.currencyService.getCurrency(Currency.RUSSIAN_CHAR_CODE);

            CurrencyCourse sourceCourse = this.currencyCourseService.getCourse(source, russianCurrency);
            if (sourceCourse == null) {
                sourceCourse = this.currencyCourseService.addTodayCourse(source);
            }

            CurrencyCourse targetCourse = this.currencyCourseService.getCourse(target, russianCurrency);
            if (targetCourse == null) {
                targetCourse = this.currencyCourseService.addTodayCourse(target);
            }

            course = this.currencyCourseService.calculateCourse(sourceCourse, targetCourse);
        }

        UserConversion conversion = null;

        try {
            this.em.getTransaction().begin();

            Double sum = amount * course.getValue();

            conversion = new UserConversion(user, course, amount, sum);

            this.em.persist(conversion);
            this.em.flush();

            this.em.getTransaction().commit();

        } catch (RuntimeException e) {
            this.em.getTransaction().rollback();
            throw e;
        }

        return conversion;
    }
}
