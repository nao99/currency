package currency.service.util;

import currency.exception.util.XmlParserException;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;

@Service
public class XmlParserService {
    /**
     * XmlParserService constructor
     */
    public XmlParserService() {}

    public Document parseXML(String xmlStr) {
        DocumentBuilderFactory builderFactory = null;
        DocumentBuilder builder = null;
        Document xml = null;

        try {
            builderFactory = DocumentBuilderFactory.newInstance();
            builder = builderFactory.newDocumentBuilder();

            InputSource source = new InputSource(
                    new StringReader(xmlStr)
            );

            xml = builder.parse(source);

        } catch (Exception e) {
            throw new XmlParserException(e.getMessage(), e.getCause());
        }

        return xml;
    }
}
