package currency.config;

import currency.entity.User;
import currency.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserPrincipalDetailsService implements UserDetailsService {
    private UserService userService;

    /**
     * UserPrincipalDetailsService constructor
     */
    @Autowired
    public UserPrincipalDetailsService(UserService userService) {
        this.userService = userService;
    }

    /**
     * Loads User by username (login)
     */
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = this.userService.getUser(s);
        if (user == null) {
            throw new UsernameNotFoundException(String.format("User with login '%s' not found", s));
        }

        return new UserPrincipal(user);
    }
}
