package currency.config;

import currency.model.dto.request.UserAuthenticateDto;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private AuthenticationManager authenticationManager;

    /**
     * AuthenticationFilter constructor
     */
    public AuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    /**
     * Attempts authenticate
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest,
                                                HttpServletResponse httpServletResponse) throws AuthenticationException {

        UserAuthenticateDto userAuthenticateDto = new UserAuthenticateDto(
                httpServletRequest.getParameter("username"),
                httpServletRequest.getParameter("password")
        );

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                userAuthenticateDto.getLogin(),
                userAuthenticateDto.getPassword(),
                new ArrayList<>()
        );

        return this.authenticationManager.authenticate(authenticationToken);
    }

    /**
     * Triggers if User was successfully authenticating
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                            FilterChain filterChain, Authentication authentication) throws IOException, ServletException {
        httpServletRequest
                .getSession()
                .setAttribute("user", authentication.getName());
    }
}
