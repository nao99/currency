package currency.config;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthorizationFilter extends BasicAuthenticationFilter {
    private UserPrincipalDetailsService userPrincipalDetailsService;

    /**
     * AuthorizationFilter constructor
     */
    public AuthorizationFilter(AuthenticationManager authenticationManager,
                               UserPrincipalDetailsService userPrincipalDetailsService) {

        super(authenticationManager);
        this.userPrincipalDetailsService = userPrincipalDetailsService;
    }

    /**
     * Does authorization
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain chain) throws IOException, ServletException {

        HttpSession session = request.getSession();
        if (session.getAttribute("user") == null) {
            chain.doFilter(request, response);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = this.getUsernamePasswordAuthenticationToken(request);
        SecurityContextHolder
                .getContext()
                .setAuthentication(authentication);

        chain.doFilter(request, response);
    }

    /**
     * Gets UsernamePasswordAuthenticationToken
     */
    private UsernamePasswordAuthenticationToken getUsernamePasswordAuthenticationToken(HttpServletRequest request) {
        String login = request.getSession().getAttribute("user").toString();

        UserPrincipal principal = (UserPrincipal) this.userPrincipalDetailsService.loadUserByUsername(login);

        return new UsernamePasswordAuthenticationToken(login, null, principal.getAuthorities());
    }
}
