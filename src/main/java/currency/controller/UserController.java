package currency.controller;

import currency.entity.Currency;
import currency.entity.User;
import currency.entity.UserConversion;
import currency.exception.CurrencyNotFoundException;
import currency.model.dto.request.UserConversionAddDto;
import currency.model.dto.request.UserConversionGetDto;
import currency.model.dto.response.UserConversionDto;
import currency.model.mapper.UserConversionMapper;
import currency.service.CurrencyService;
import currency.service.SecurityService;
import currency.service.UserConversionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(path = "/currency/api/v1.0/users")
public class UserController {
    private SecurityService securityService;

    private CurrencyService currencyService;

    private UserConversionService userConversionService;

    private Validator validator;

    /**
     * UserController constructor
     */
    @Autowired
    public UserController(SecurityService securityService, CurrencyService currencyService,
                          UserConversionService userConversionService, ValidatorFactory validatorFactory) {

        this.securityService = securityService;
        this.currencyService = currencyService;
        this.userConversionService = userConversionService;
        this.validator = validatorFactory.getValidator();
    }

    @GetMapping(value = "/conversions/")
    @Secured(value = "ROLE_USER")
    @ResponseBody
    public ResponseEntity<List<UserConversionDto>> getConversions(
            @RequestBody UserConversionGetDto queryDto) {

        User currentUser = this.securityService.getCurrentUser();
        queryDto.setUser(currentUser);

        List<UserConversion> conversions = this.userConversionService.getConversions(queryDto);

        List<UserConversionDto> conversionsJson = new ArrayList<>();
        conversions.forEach(conversion -> {
            conversionsJson.add(
                    UserConversionMapper.userConversionToUserConversionDto(conversion)
            );
        });

        return new ResponseEntity<>(conversionsJson, HttpStatus.OK);
    }

    @PostMapping(value = "/conversions/")
    @Secured(value = "ROLE_USER")
    @ResponseBody
    public ResponseEntity<UserConversionDto> addConversion(@RequestBody UserConversionAddDto createDto) {

        Set<ConstraintViolation<UserConversionAddDto>> violations = this.validator.validate(createDto);
        if (!violations.isEmpty()) {
            throw new ValidationException(validator.toString());
        }

        // Need to move it to UserConversionController
        User user = this.securityService.getCurrentUser();
        Currency source = this.currencyService.getCurrency(createDto.getSourceId());
        Currency target = this.currencyService.getCurrency(createDto.getTargetId());

        if (source == null || target == null) {
            throw new CurrencyNotFoundException("Currency not found");
        }

        UserConversion conversion = this.userConversionService.addConversion(user, source, target, createDto.getAmount());

        UserConversionDto conversionJson = UserConversionMapper.userConversionToUserConversionDto(conversion);

        return new ResponseEntity<>(conversionJson, HttpStatus.OK);
    }
}
