package currency.controller;

import currency.entity.Currency;
import currency.model.dto.response.CurrencyDto;
import currency.model.mapper.CurrencyMapper;
import currency.service.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/currency/api/v1.0/currencies")
public class CurrencyController {
    private CurrencyService currencyService;

    /**
     * UserController constructor
     */
    @Autowired
    public CurrencyController(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    @GetMapping(value = "/")
    @Secured(value = "ROLE_USER")
    @ResponseBody
    public ResponseEntity<List<CurrencyDto>> getCurrencies() {
        Iterable<Currency> currencies = this.currencyService.getCurrencies();

        List<CurrencyDto> currenciesJson = new ArrayList<>();
        currencies.forEach(currency -> {
            currenciesJson.add(
                    CurrencyMapper.currencyToCurrencyDto(currency)
            );
        });

        return new ResponseEntity<>(currenciesJson, HttpStatus.OK);
    }
}
