package currency.job;

import currency.entity.Currency;
import currency.entity.CurrencyCourse;
import currency.model.dto.request.CurrencyAddDto;
import currency.model.util.DateTimeRange;
import currency.service.CurrencyCourseService;
import currency.service.CurrencyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

import java.util.List;
import java.util.Map;

@Component
public class InitApplicationJob implements Runnable {
    private CurrencyService currencyService;

    private CurrencyCourseService currencyCourseService;

    private Logger logger;

    /**
     * InitApplicationJob constructor
     */
    @Autowired
    public InitApplicationJob(CurrencyService currencyService, CurrencyCourseService currencyCourseService) {

        this.currencyService = currencyService;
        this.currencyCourseService = currencyCourseService;
        this.logger = LoggerFactory.getLogger("currency.InitApplicationJob");
    }

    /**
     * Method that gets a currencies and their course
     * and write its to the database
     */
    public void run() {
        this.logger.info("Start getting of new currencies and courses");

        try {
            this.addNewCurrencies();

        } catch (RuntimeException e) {
            this.logger.error(e.getMessage());
        }

        this.addNewCourses();
    }

    /**
     * Adds to the database a new Currencies if its exits
     */
    private void addNewCurrencies() {
        Document xml = this.currencyService.getCurrenciesXml();
        Map<String, CurrencyAddDto> addDtos = this.currencyService.getCurrenciesByXml(xml);
        Iterable<Currency> currencies = this.currencyService.getCurrencies();

        List<CurrencyAddDto> newCurrencies = this.currencyService.defineNewCurrencies(currencies, addDtos);

        this.logger.info(String.format("Found %d new currencies", newCurrencies.size()));

        this.currencyService.addCurrencies(newCurrencies);

        this.logger.info("Currencies successfully added");
    }

    /**
     * Adds to the database a new CurrencyCourses if its exits
     */
    private void addNewCourses() {
        DateTimeRange range = DateTimeRange.getOneDayRange();

        List<Currency> currencies = this.currencyService.getExpiredCurrencies(range.getStartAt());

        currencies.forEach(currency -> {
            try {
                this.logger.info(String.format("Start adding of today course for %d currency", currency.getId()));

                CurrencyCourse course = this.currencyCourseService.addTodayCourse(currency);

                this.logger.info(String.format("Course %d successfully added", course.getId()));

            } catch (RuntimeException e) {
                this.logger.error(e.getMessage());
            }
        });
    }
}
