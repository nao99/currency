package currency.model.util;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Class that keeps a date interval
 */
public class DateTimeRange {
    public static final String DD_MM_YYY_SLASH_FORMAT = "dd/MM/yyyy";
    public static final String DD_MM_YYY_DOT_FORMAT   = "dd.MM.yyyy";

    private Date startAt;

    private Date endAt;

    public DateTimeRange(Date startAt, Date endAt) {
        this.startAt = startAt;
        this.endAt = endAt;
    }

    public Date getStartAt() {
        return this.startAt;
    }

    public DateTimeRange setStartAt(Date startAt) {
        this.startAt = startAt;
        return this;
    }

    public Date getEndAt() {
        return this.endAt;
    }

    public DateTimeRange setEndAt(Date endAt) {
        this.endAt = endAt;
        return this;
    }

    /**
     * Gets the interval from today to tomorrow
     * by one day
     */
    public static DateTimeRange getOneDayRange() {
        LocalDate localDate = LocalDate.now();

        Date startAt = Date.from(
                localDate.atStartOfDay(ZoneId.systemDefault()).toInstant()
        );
        Date endAt = Date.from(
                localDate.plusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant()
        );

        return new DateTimeRange(startAt, endAt);
    }

    public Map<String, String> toFormattedStringRange(String format) {
        Map<String, String> formattedStringRange = new HashMap<>();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);

        formattedStringRange.put("startAt", simpleDateFormat.format(this.startAt));
        formattedStringRange.put("endAt", simpleDateFormat.format(this.endAt));

        return formattedStringRange;
    }
}
