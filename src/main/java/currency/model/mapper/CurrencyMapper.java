package currency.model.mapper;

import currency.entity.Currency;
import currency.model.dto.response.CurrencyDto;

public class CurrencyMapper {
    /**
     * Converts Currency to CurrencyDto
     */
    public static CurrencyDto currencyToCurrencyDto(Currency currency) {
        return new CurrencyDto(
                currency.getId(),
                currency.getCurrencyId(),
                currency.getNumCode(),
                currency.getCharCode(),
                currency.getName()
        );
    }
}
