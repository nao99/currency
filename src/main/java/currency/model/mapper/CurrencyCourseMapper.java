package currency.model.mapper;

import currency.entity.CurrencyCourse;
import currency.model.dto.response.CurrencyCourseDto;
import currency.model.dto.response.CurrencyDto;

public class CurrencyCourseMapper {
    /**
     * Converts CurrencyCourse to CurrencyCourseDto
     */
    public static CurrencyCourseDto currencyCourseToCurrencyCourseDto(CurrencyCourse course) {
        CurrencyDto source = CurrencyMapper.currencyToCurrencyDto(course.getSource());
        CurrencyDto target = CurrencyMapper.currencyToCurrencyDto(course.getTarget());

        return new CurrencyCourseDto(
                course.getId(),
                source,
                target,
                course.getValue(),
                course.getCreatedAt()
        );
    }
}
