package currency.model.mapper;

import currency.model.dto.response.RoleDto;
import currency.entity.Role;

public class RoleMapper {
    /**
     * Converts Role to RoleDto
     */
    public static RoleDto roleToRoleDto(Role role) {
        return new RoleDto(
                role.getId(),
                role.getName()
        );
    }
}
