package currency.model.mapper;

import currency.entity.User;
import currency.model.dto.response.RoleDto;
import currency.model.dto.response.UserDto;

import java.util.ArrayList;
import java.util.List;

public class UserMapper {
    /**
     * Converts User to UserDto
     */
    public static UserDto userToUserDto(User user) {
        List<RoleDto> roles = new ArrayList<>();
        user.getRoles().forEach(userRole -> {
            roles.add(
                    RoleMapper.roleToRoleDto(userRole.getRole())
            );
        });

        return new UserDto(
                user.getId(),
                user.getLogin(),
                roles
        );
    }
}
