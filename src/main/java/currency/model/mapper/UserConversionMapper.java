package currency.model.mapper;

import currency.entity.UserConversion;
import currency.model.dto.response.CurrencyCourseDto;
import currency.model.dto.response.UserConversionDto;
import currency.model.dto.response.UserDto;

public class UserConversionMapper {
    /**
     * Converts UserConversion to UserConversionDto
     */
    public static UserConversionDto userConversionToUserConversionDto(UserConversion conversion) {
        UserDto user = UserMapper.userToUserDto(conversion.getUser());
        CurrencyCourseDto course = CurrencyCourseMapper.currencyCourseToCurrencyCourseDto(conversion.getCourse());

        return new UserConversionDto(
                conversion.getId(),
                user,
                course,
                conversion.getAmount(),
                conversion.getSum(),
                conversion.getCreatedAt()
        );
    }
}
