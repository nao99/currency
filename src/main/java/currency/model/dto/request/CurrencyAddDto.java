package currency.model.dto.request;

import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Validated
public class CurrencyAddDto {
    @NotNull
    @Size(min = 6, max = 8)
    private String currencyId;

    @NotNull
    @Size(min = 3, max = 3)
    private String numCode;

    @NotNull
    @Size(min = 3, max = 3)
    private String charCode;

    @NotNull
    @Size(min = 2, max = 65)
    private String name;

    /**
     * CurrencyAddDto constructor
     */
    public CurrencyAddDto(String currencyId, String numCode, String charCode, String name) {
        this.currencyId = currencyId;
        this.numCode = numCode;
        this.charCode = charCode;
        this.name = name;
    }

    public String getCurrencyId() {
        return this.currencyId;
    }

    public CurrencyAddDto setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
        return this;
    }

    public String getNumCode() {
        return this.numCode;
    }

    public CurrencyAddDto setNumCode(String numCode) {
        this.numCode = numCode;
        return this;
    }

    public String getCharCode() {
        return this.charCode;
    }

    public CurrencyAddDto setCharCode(String charCode) {
        this.charCode = charCode;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public CurrencyAddDto setName(String name) {
        this.name = name;
        return this;
    }
}
