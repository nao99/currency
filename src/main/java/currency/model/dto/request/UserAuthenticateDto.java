package currency.model.dto.request;

/**
 * Contains the User information
 * for his authentication
 */
public class UserAuthenticateDto {
    private String login;

    private String password;

    /**
     * UserAuthenticateDto constructor
     */
    public UserAuthenticateDto(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return this.login;
    }

    public UserAuthenticateDto setLogin(String login) {
        this.login = login;

        return this;
    }

    public String getPassword() {
        return this.password;
    }

    public UserAuthenticateDto setPassword(String password) {
        this.password = password;

        return this;
    }
}
