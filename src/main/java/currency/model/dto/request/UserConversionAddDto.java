package currency.model.dto.request;

import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

@Validated
public class UserConversionAddDto {
    @NotNull
    private Integer sourceId;

    @NotNull
    private Integer targetId;

    @NotNull
    private Double amount;

    /**
     * UserConversionAddDto default constructor
     */
    public UserConversionAddDto() {}

    /**
     * UserConversionAddDto constructor
     */
    public UserConversionAddDto(Integer sourceId, Integer targetId, Double amount) {
        this.sourceId = sourceId;
        this.targetId = targetId;
        this.amount = amount;
    }

    public Integer getSourceId() {
        return this.sourceId;
    }

    public UserConversionAddDto setSourceId(Integer sourceId) {
        this.sourceId = sourceId;

        return this;
    }

    public Integer getTargetId() {
        return this.targetId;
    }

    public UserConversionAddDto setTargetId(Integer targetId) {
        this.targetId = targetId;

        return this;
    }

    public Double getAmount() {
        return this.amount;
    }

    public UserConversionAddDto setAmount(Double amount) {
        this.amount = amount;

        return this;
    }
}
