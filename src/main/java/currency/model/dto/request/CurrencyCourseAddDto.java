package currency.model.dto.request;

import currency.entity.Currency;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Validated
public class CurrencyCourseAddDto {
    @NotNull
    private Currency source;

    @NotNull
    private Currency target;

    @NotNull
    private Double value;

    @NotNull
    private Date createdAt;

    /**
     * CurrencyCourseAddDto constructor
     */
    public CurrencyCourseAddDto(Currency source, Currency target, Double value, Date createdAt) {
        this.source = source;
        this.target = target;
        this.value = value;
        this.createdAt = createdAt;
    }

    public Currency getSource() {
        return this.source;
    }

    public CurrencyCourseAddDto setSource(Currency source) {
        this.source = source;

        return this;
    }

    public Currency getTarget() {
        return this.target;
    }

    public CurrencyCourseAddDto setTarget(Currency target) {
        this.target = target;

        return this;
    }

    public Double getValue() {
        return this.value;
    }

    public CurrencyCourseAddDto setValue(Double value) {
        this.value = value;

        return this;
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public CurrencyCourseAddDto setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;

        return this;
    }
}
