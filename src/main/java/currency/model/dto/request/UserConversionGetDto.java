package currency.model.dto.request;

import currency.entity.CurrencyCourse;
import currency.entity.User;

import java.util.Date;

public class UserConversionGetDto {
    private User user;

    private CurrencyCourse currencyCourse;

    private Double amount;

    private Double sum;

    private Date createdAt;

    private Integer limit = 50;

    private Integer offset = 0;

    /**
     * UserConversionGetDto constructor
     */
    public UserConversionGetDto(User user, CurrencyCourse currencyCourse, Double amount, Double sum, Date createdAt,
                                Integer limit, Integer offset) {
        this.user = user;
        this.currencyCourse = currencyCourse;
        this.amount = amount;
        this.sum = sum;
        this.createdAt = createdAt;

        if (limit != null) {
            this.limit = limit;
        }

        if (offset != null) {
            this.offset = offset;
        }
    }

    public User getUser() {
        return this.user;
    }

    public UserConversionGetDto setUser(User user) {
        this.user = user;

        return this;
    }

    public CurrencyCourse getCurrencyCourse() {
        return this.currencyCourse;
    }

    public UserConversionGetDto setCurrencyCourse(CurrencyCourse currencyCourse) {
        this.currencyCourse = currencyCourse;

        return this;
    }

    public Double getAmount() {
        return this.amount;
    }

    public UserConversionGetDto setAmount(Double amount) {
        this.amount = amount;

        return this;
    }

    public Double getSum() {
        return this.sum;
    }

    public UserConversionGetDto setSum(Double sum) {
        this.sum = sum;

        return this;
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public UserConversionGetDto setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;

        return this;
    }

    public Integer getLimit() {
        return this.limit;
    }

    public UserConversionGetDto setLimit(Integer limit) {
        this.limit = limit;

        return this;
    }

    public Integer getOffset() {
        return this.offset;
    }

    public UserConversionGetDto setOffset(Integer offset) {
        this.offset = offset;

        return this;
    }
}
