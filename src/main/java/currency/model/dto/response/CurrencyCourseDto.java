package currency.model.dto.response;

import java.util.Date;

public class CurrencyCourseDto {
    private Integer id;

    private CurrencyDto source;

    private CurrencyDto target;

    private Double value;

    private Date createdAt;

    /**
     * CurrencyCourseDto constructor
     */
    public CurrencyCourseDto(Integer id, CurrencyDto source, CurrencyDto target, Double value, Date createdAt) {
        this.id = id;
        this.source = source;
        this.target = target;
        this.value = value;
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return this.id;
    }

    public CurrencyCourseDto setId(Integer id) {
        this.id = id;

        return this;
    }

    public CurrencyDto getSource() {
        return this.source;
    }

    public CurrencyCourseDto setSource(CurrencyDto source) {
        this.source = source;

        return this;
    }

    public CurrencyDto getTarget() {
        return this.target;
    }

    public CurrencyCourseDto setTarget(CurrencyDto target) {
        this.target = target;

        return this;
    }

    public Double getValue() {
        return this.value;
    }

    public CurrencyCourseDto setValue(Double value) {
        this.value = value;

        return this;
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public CurrencyCourseDto setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;

        return this;
    }
}
