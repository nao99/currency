package currency.model.dto.response;

import java.util.List;

public class UserDto {
    private Integer id;

    private String login;

    private List<RoleDto> roles;

    /**
     * UserDto constructor
     */
    public UserDto(Integer id, String login, List<RoleDto> roles) {
        this.id = id;
        this.login = login;
        this.roles = roles;
    }

    public Integer getId() {
        return this.id;
    }

    public UserDto setId(Integer id) {
        this.id = id;

        return this;
    }

    public String getLogin() {
        return this.login;
    }

    public UserDto setLogin(String login) {
        this.login = login;

        return this;
    }

    public List<RoleDto> getRoles() {
        return this.roles;
    }

    public UserDto setRoles(List<RoleDto> roles) {
        this.roles = roles;

        return this;
    }
}
