package currency.model.dto.response;

public class RoleDto {
    private Integer id;

    private String name;

    /**
     * RoleDto constructor
     */
    public RoleDto(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return this.id;
    }

    public RoleDto setId(Integer id) {
        this.id = id;

        return this;
    }

    public String getName() {
        return this.name;
    }

    public RoleDto setName(String name) {
        this.name = name;

        return this;
    }
}
