package currency.model.dto.response;

public class CurrencyDto {
    private Integer id;

    private String currencyId;

    private String numCode;

    private String charCode;

    private String name;

    /**
     * CurrencyDto constructor
     */
    public CurrencyDto(Integer id, String currencyId, String numCode, String charCode, String name) {
        this.id = id;
        this.currencyId = currencyId;
        this.numCode = numCode;
        this.charCode = charCode;
        this.name = name;
    }

    public Integer getId() {
        return this.id;
    }

    public CurrencyDto setId(Integer id) {
        this.id = id;

        return this;
    }

    public String getCurrencyId() {
        return this.currencyId;
    }

    public CurrencyDto setCurrencyId(String currencyId) {
        this.currencyId = currencyId;

        return this;
    }

    public String getNumCode() {
        return this.numCode;
    }

    public CurrencyDto setNumCode(String numCode) {
        this.numCode = numCode;

        return this;
    }

    public String getCharCode() {
        return this.charCode;
    }

    public CurrencyDto setCharCode(String charCode) {
        this.charCode = charCode;

        return this;
    }

    public String getName() {
        return this.name;
    }

    public CurrencyDto setName(String name) {
        this.name = name;

        return this;
    }
}
