package currency.model.dto.response;

import java.util.Date;

public class UserConversionDto {
    private Integer id;

    private UserDto user;

    private CurrencyCourseDto currencyCourse;

    private Double amount;

    private Double sum;

    private Date createdAt;

    /**
     * UserConversionDto constructor
     */
    public UserConversionDto(Integer id, UserDto user, CurrencyCourseDto currencyCourse, Double amount, Double sum,
                             Date createdAt) {
        this.id = id;
        this.user = user;
        this.currencyCourse = currencyCourse;
        this.amount = amount;
        this.sum = sum;
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return this.id;
    }

    public UserConversionDto setId(Integer id) {
        this.id = id;

        return this;
    }

    public UserDto getUser() {
        return this.user;
    }

    public UserConversionDto setUser(UserDto user) {
        this.user = user;

        return this;
    }

    public CurrencyCourseDto getCurrencyCourse() {
        return this.currencyCourse;
    }

    public UserConversionDto setCurrencyCourse(CurrencyCourseDto currencyCourse) {
        this.currencyCourse = currencyCourse;

        return this;
    }

    public Double getAmount() {
        return this.amount;
    }

    public UserConversionDto setAmount(Double amount) {
        this.amount = amount;

        return this;
    }

    public Double getSum() {
        return this.sum;
    }

    public UserConversionDto setSum(Double sum) {
        this.sum = sum;

        return this;
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public UserConversionDto setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;

        return this;
    }
}
