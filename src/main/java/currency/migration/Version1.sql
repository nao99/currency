create table currency(
    id serial primary key,
    currency_id varchar(8) not null,
    num_code varchar(3) not null,
    char_code varchar(3) not null,
    name varchar(65) not null
);

create table currency_course(
    id serial primary key,
    source_id integer not null
        constraint FK_currency_course_source__currency
            references currency(id)
                on delete cascade
                on update restrict,
    target_id integer not null
        constraint FK_currency_course_target__currency
                references currency(id)
                    on delete cascade
                    on update restrict,
    value double precision not null,
    created_at date default current_date
);

create table "user"(
    id serial primary key,
    login varchar(16) not null,
    password varchar(65) not null
);

create table user_conversion(
    id serial primary key,
    user_id integer not null
        constraint FK_user_conversion__user
            references "user"(id)
                on delete cascade
                on update restrict,
    currency_course_id integer not null
        constraint FK_user_conversion__currency_course
            references currency_course(id)
                on delete restrict
                on update restrict,
    amount double precision not null,
    sum double precision not null,
    created_at timestamp default current_timestamp
);
