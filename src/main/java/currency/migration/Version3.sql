/* Manually insert into currency the RUB currency, roles, user and his roles */

insert into currency (currency_id, num_code, char_code, "name")
    values ('R01000', '643', 'RUB', 'Russian ruble');

insert into role (name)
    values ('ROLE_ADMIN');

insert into role (name)
    values ('ROLE_USER');

/* password - helloworld */
insert into "user" (login, password)
    values ('testuser', '$2a$10$DI5x0UGU5MZQhh1xbLqYkubn0/upI3q/tm/NMET4OL75itoquNfMG');

insert into user_role (user_id, role_id)
    values (1, 2);
