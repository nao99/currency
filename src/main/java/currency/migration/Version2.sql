create table role(
    id serial primary key,
    name varchar(65) not null
);

create table user_role(
    id serial primary key,
    user_id integer not null
        constraint FK_user_role__user
            references "user"(id)
                on delete cascade
                on update restrict,
    role_id integer not null
        constraint FK_user_role__role
            references role(id)
                on delete restrict
                on update restrict
);
