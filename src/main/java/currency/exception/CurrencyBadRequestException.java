package currency.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class CurrencyBadRequestException extends RuntimeException {
    /**
     * CurrencyBadRequestException empty constructor
     */
    public CurrencyBadRequestException() {
        super();
    }

    /**
     * CurrencyBadRequestException with message constructor
     */
    public CurrencyBadRequestException(String message) {
        super(message);
    }

    /**
     * CurrencyBadRequestException with cause constructor
     */
    public CurrencyBadRequestException(Throwable cause) {
        super(cause);
    }

    /**
     * CurrencyBadRequestException with message and cause constructor
     */
    public CurrencyBadRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
