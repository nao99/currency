package currency.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class CurrencyConflictException extends RuntimeException {
    /**
     * CurrencyConflictException empty constructor
     */
    public CurrencyConflictException() {
        super();
    }

    /**
     * CurrencyConflictException with message constructor
     */
    public CurrencyConflictException(String message) {
        super(message);
    }

    /**
     * CurrencyConflictException with cause constructor
     */
    public CurrencyConflictException(Throwable cause) {
        super(cause);
    }

    /**
     * CurrencyConflictException with message and cause constructor
     */
    public CurrencyConflictException(String message, Throwable cause) {
        super(message, cause);
    }
}
