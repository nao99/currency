package currency.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class CurrencyNotFoundException extends RuntimeException {
    /**
     * CurrencyNotFoundException empty constructor
     */
    public CurrencyNotFoundException() {
        super();
    }

    /**
     * CurrencyNotFoundException with message constructor
     */
    public CurrencyNotFoundException(String message) {
        super(message);
    }

    /**
     * CurrencyNotFoundException with cause constructor
     */
    public CurrencyNotFoundException(Throwable cause) {
        super(cause);
    }

    /**
     * CurrencyNotFoundException with message and cause constructor
     */
    public CurrencyNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
