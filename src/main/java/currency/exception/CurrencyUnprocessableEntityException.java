package currency.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
public class CurrencyUnprocessableEntityException extends RuntimeException {
    /**
     * CurrencyUnprocessableEntityException empty constructor
     */
    public CurrencyUnprocessableEntityException() {
        super();
    }

    /**
     * CurrencyUnprocessableEntityException with message constructor
     */
    public CurrencyUnprocessableEntityException(String message) {
        super(message);
    }

    /**
     * CurrencyUnprocessableEntityException with cause constructor
     */
    public CurrencyUnprocessableEntityException(Throwable cause) {
        super(cause);
    }

    /**
     * CurrencyUnprocessableEntityException with message and cause constructor
     */
    public CurrencyUnprocessableEntityException(String message, Throwable cause) {
        super(message, cause);
    }
}
