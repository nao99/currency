package currency.exception.util;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
public class XmlParserException extends RuntimeException {
    /**
     * XmlParserException empty constructor
     */
    public XmlParserException() {
        super();
    }

    /**
     * XmlParserException with message constructor
     */
    public XmlParserException(String message) {
        super(message);
    }

    /**
     * XmlParserException with cause constructor
     */
    public XmlParserException(Throwable cause) {
        super(cause);
    }

    /**
     * XmlParserException with message and cause constructor
     */
    public XmlParserException(String message, Throwable cause) {
        super(message, cause);
    }
}
