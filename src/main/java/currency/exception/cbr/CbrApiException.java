package currency.exception.cbr;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE)
public class CbrApiException extends RuntimeException {
    /**
     * CbrApiException empty constructor
     */
    public CbrApiException() {
        super();
    }

    /**
     * CbrApiException with message constructor
     */
    public CbrApiException(String message) {
        super(message);
    }

    /**
     * CbrApiException with cause constructor
     */
    public CbrApiException(Throwable cause) {
        super(cause);
    }

    /**
     * CbrApiException with message and cause constructor
     */
    public CbrApiException(String message, Throwable cause) {
        super(message, cause);
    }
}
