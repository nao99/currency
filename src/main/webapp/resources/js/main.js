$(function () {
    $.ajax({
        url: '/currency/api/v1.0/currencies/',
        method: 'GET',
        dataType: 'json',
        success: function (response) {
            let $converterSelects = $('.converter select');
            $.each($converterSelects, function (index, item) {
                $.each(response, function (index, currency) {
                    let currencyOption = '<option value="' + currency.id + '">' + currency.name + '</option>';
                    $(item).append(currencyOption);
                });
            });
        }
    });

    $(document).on('submit', '#converter-form', function (e) {
        e.preventDefault();

        let $this = $(this);

        let sourceId = $this.find('.source-currency-choose').val();
        let targetId = $this.find('.target-currency-choose').val();
        let sum = $this.find('.source-currency-sum').val();

        $.ajax({
            url: '/currency/api/v1.0/users/conversions/',
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                sourceId: sourceId,
                targetId: targetId,
                sum: sum,
            }),
            dataType: 'json',
            success: function (response) {
                console.log(response);
            }
        });
    });
});
