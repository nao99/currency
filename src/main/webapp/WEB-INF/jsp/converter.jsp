<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Converter</title>

    <spring:url value="/resources/css/main.css" var="mainCss"/>
    <spring:url value="/resources/js/jquery.js" var="jqueryJs"/>
    <spring:url value="/resources/js/main.js" var="mainJs"/>


    <link href="${mainCss}" rel="stylesheet"/>
    <script src="${jqueryJs}"></script>
    <script src="${mainJs}"></script>
</head>

<body>

<form id="converter-form">
    <div class="converter">
        <select class="source-currency-choose"></select>
        <select class="target-currency-choose"></select>

        <input class="source-currency-sum" type="text" placeholder="Enter the source sum">
        <input class="target-currency-sum" type="text" placeholder="Sum" disabled>

        <button class="btn-convert">Convert</button>
    </div>
</form>


<div class="conversions">
    <%--<jsp:include page="conversion.jsp">--%>
</div>

</body>
</html>
